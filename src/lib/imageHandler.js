const threshold = 20; // Define your threshold value here
const isWhitePixel = (r, g, b) => {
  const diffR = Math.abs(r - 255);
  const diffG = Math.abs(g - 255);
  const diffB = Math.abs(b - 255);
  return diffR <= threshold && diffG <= threshold && diffB <= threshold;
};

export function convertImageToBlack(image) {
  const canvas = document.createElement("canvas");
  const ctx = canvas.getContext("2d");
  canvas.width = image.width;
  canvas.height = image.height;

  ctx.drawImage(image, 0, 0);

  const imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);
  const data = imageData.data;

  for (let i = 0; i < data.length; i += 4) {
    const r = data[i];
    const g = data[i + 1];
    const b = data[i + 2];

    const binary = isWhitePixel(r, g, b) ? 255 : 0;

    data[i] = binary;
    data[i + 1] = binary;
    data[i + 2] = binary;
  }

  ctx.putImageData(imageData, 0, 0);

  return canvas.toDataURL("image/jpeg");
}
