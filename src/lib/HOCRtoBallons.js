const HOCRtoBallons = (hocr) => {
  const positions = [];

  // Parse hOCR data and extract bounding box positions and related text
  const parser = new DOMParser();
  const doc = parser.parseFromString(hocr, "text/html");
  const wordElements = doc.querySelectorAll(".ocrx_word");

  wordElements.forEach((wordElement) => {
    const title = wordElement.getAttribute("title");
    const bboxMatch = title.match(/bbox (\d+) (\d+) (\d+) (\d+)/);
    if (bboxMatch) {
      const [_, x1, y1, x2, y2] = bboxMatch.map(Number);

      const text = wordElement.innerText.trim();
      positions.push({ x1, y1, x2, y2, text });
    }
  });

  const ballons = positions.map(({ x1, y1, x2, y2, text }) => ({
    x1,
    y1,
    x2,
    y2,
    text,
  }));

  return ballons;
};

export default HOCRtoBallons;
