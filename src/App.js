import React, { useEffect, useState, useRef } from "react";
import Tesseract from "tesseract.js";
import HOCRtoBallons from "./lib/HOCRtoBallons";
import { convertImageToBlack } from "./lib/imageHandler";
import axios from "axios";
import Modal from "./components/Modal";
import colors from "./colors";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCog } from "@fortawesome/free-solid-svg-icons";
import config from "./config";
import languages from "./lib/tesseractLanguages";

function App() {
  const [imageUrls, setImageUrls] = useState([]);
  const [page, setPage] = useState(1);
  const [isSettingsOpen, setIsSettingOpen] = useState(false);
  const [isDictionaryOpen, setIsDictionaryOpen] = useState(false);
  const [selectedWord, setSelectedWord] = useState("");

  // settings
  const [language, setLanguage] = useState("eng");
  const [manwhaUrl, setManwhaUrl] = useState("");
  const [dictionaryUrl, setDictionaryUrl] = useState("");

  useEffect(() => {
    const localLanguage = localStorage.getItem("LANGUAGE");
    const localManwhaUrl = localStorage.getItem("MANWHA_URL");
    const localDictionaryUrl = localStorage.getItem("DICTIONARY_URL");
    let missingSetupStr = "";

    if (!localLanguage) missingSetupStr += ", language";
    else setLanguage(localLanguage);

    if (!localManwhaUrl) missingSetupStr += ", manwha url";
    else setManwhaUrl(localManwhaUrl);

    if (!localDictionaryUrl) missingSetupStr += ", dictionary url";
    else setDictionaryUrl(localDictionaryUrl);

    if (missingSetupStr.length != 0) {
      alert("You need to setup:" + missingSetupStr.substring(1));
      setIsSettingOpen(true);
    }
  }, []);

  useEffect(() => {
    if (manwhaUrl) {
      axios
        .get(`${config.server}/getImagesFromUrl`, {
          params: {
            url: manwhaUrl,
          },
        })
        .then((res) => {
          setImageUrls(res.data.imageUrls);
        });
    }
  }, [manwhaUrl]);

  useEffect(() => {
    if (imageUrls) {
      const handleScroll = () => {
        const { scrollTop, clientHeight, scrollHeight } =
          document.documentElement;
        if (scrollTop + clientHeight >= scrollHeight) {
          setPage((page) => page + 1);
        }
      };

      // Attach the scroll event listener
      window.addEventListener("scroll", handleScroll);

      return () => {
        window.removeEventListener("scroll", handleScroll);
      };
    }
  }, [imageUrls]);

  useEffect(() => {
    if (selectedWord) {
      setIsDictionaryOpen(true);
    }
  }, [selectedWord]);

  return (
    <div
      className="App"
      style={{
        backgroundColor: "#000",
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
      }}
    >
      {isSettingsOpen && (
        <SettingsModal
          setIsOpen={setIsSettingOpen}
          language={language}
          setLanguage={setLanguage}
          manwhaUrl={manwhaUrl}
          setManwhaUrl={setManwhaUrl}
          dictionaryUrl={dictionaryUrl}
          setDictionaryUrl={setDictionaryUrl}
        />
      )}
      {dictionaryUrl && (
        <DictionaryModal
          setIsOpen={setIsDictionaryOpen}
          word={selectedWord}
          dictionaryUrl={dictionaryUrl}
          isOpen={isDictionaryOpen}
        />
      )}
      <div
        style={{
          backgroundColor: colors.gray,
          color: colors.white,
          width: "100%",
          padding: "10px 0px",
          cursor: "pointer",
          display: "flex",
          fontSize: 32,
          justifyContent: "center",
          alignItems: "center",
        }}
        onClick={() => setIsSettingOpen(true)}
      >
        <FontAwesomeIcon
          icon={faCog}
          style={{ width: 48, height: 48, color: colors.white, marginLeft: 20 }}
        />
        <label style={{ marginLeft: 10 }}>settings</label>
      </div>
      {imageUrls.slice(0, page * 10).map((url, i) => (
        <ImageTreatingBallon
          key={"image_" + url}
          manwhaPage={i}
          setSelectedWord={setSelectedWord}
          language={language}
        />
      ))}
      <div style={{ display: "flex", width: "100%" }}>
        <button
          style={{
            flex: 1,
            backgroundColor: colors.gray,
            color: colors.white,
            ...buttonStyles,
          }}
          onClick={() => {
            changeManwhaUrlPageAddingNumber(-1, setManwhaUrl);
          }}
        >
          previous
        </button>
        <button
          style={{
            flex: 1,
            backgroundColor: colors.white,
            color: colors.gray,
            ...buttonStyles,
          }}
          onClick={() => {
            changeManwhaUrlPageAddingNumber(1, setManwhaUrl);
          }}
        >
          next
        </button>
      </div>
    </div>
  );
}

const changeManwhaUrlPageAddingNumber = (number, setManwhaUrl) => {
  setManwhaUrl((manwhaUrl) => {
    const splittedUrl = manwhaUrl.split("/");
    const numberPartOfUrl = splittedUrl[splittedUrl.length - 1];
    const newPage = Number(numberPartOfUrl) + number;
    const newUrl =
      manwhaUrl.substring(0, manwhaUrl.length - numberPartOfUrl.length) +
      newPage;
    localStorage.setItem("MANWHA_URL", newUrl);

    return newUrl;
  });
};

const buttonStyles = {
  fontSize: 24,
  padding: 10,
  fontWeight: "bolder",
  cursor: "pointer",
};

const ImageTreatingBallon = ({ manwhaPage, setSelectedWord, language }) => {
  const [ballons, setBallons] = useState([]);
  // const [blackImage, setBlackImage] = useState();

  const convertedToBinaryStartedRef = useRef(false);

  useEffect(() => {
    const convertToBinary = async () => {
      console.log("aaa");
      convertedToBinaryStartedRef.current = true;

      try {
        const image = new Image();
        image.crossOrigin = "Anonymous";

        image.src = `${config.server}/proxy-image?page=${manwhaPage} `;

        image.onload = () => {
          const imageDataUrl = convertImageToBlack(image);
          // setBlackImage(imageDataUrl);

          Tesseract.recognize(imageDataUrl, language, {}).then(
            ({ data: { text, hocr } }) => {
              setBallons(HOCRtoBallons(hocr, 0));
            }
          );
        };
      } catch (error) {
        console.error("Error processing image:", error);
      }
    };

    // avoid dupliclate bug
    if (!convertedToBinaryStartedRef.current) convertToBinary();
  }, [language]);

  return (
    <div style={{ position: "relative" }}>
      <img
        src={`${config.server}/proxy-image?page=${manwhaPage}`}
        alt="manwha's page"
      />
      {ballons.map(({ x1, x2, y1, y2, text }) => {
        return (
          <div
            key={`ballon_${x1}-${y1}-${x2}-${y2}`}
            style={{
              position: "absolute",
              backgroundColor: "rgba(0,0,0,.1)",
              left: x1,
              top: y1,
              width: x2 - x1,
              height: y2 - y1,
              cursor: "pointer",
            }}
            onClick={() =>
              setSelectedWord(text.toLowerCase().replace(/[^\w\sÀ-ÿ]/g, ""))
            }
          />
        );
      })}
    </div>
  );
};

const inputStyles = { padding: "0px 5px", marginTop: 10, fontSize: 32 };
const labelStyles = { marginTop: 20 };

function SettingsModal({
  setIsOpen,
  language,
  setLanguage,
  manwhaUrl,
  setManwhaUrl,
  dictionaryUrl,
  setDictionaryUrl,
}) {
  const [tempLanguage, setTempLanguage] = useState(language);
  const [tempManwhaUrl, setTempManwhaUrl] = useState(manwhaUrl);
  const [tempDictionaryUrl, setTempDictionaryUrl] = useState(dictionaryUrl);

  return (
    <Modal setIsOpen={setIsOpen} width="60%">
      <div
        style={{
          backgroundColor: colors.gray,
          color: colors.white,
          fontSize: 32,
          display: "flex",
          flexDirection: "column",
          padding: 20,
        }}
      >
        <label style={labelStyles}>Language:</label>
        <select
          id="languageSelect"
          value={tempLanguage}
          style={inputStyles}
          onChange={(ev) => setTempLanguage(ev.target.value)}
        >
          {languages.map(({ code, name }) => (
            <option value={code}>{name}</option>
          ))}
        </select>

        <label style={labelStyles}>Manwha url:</label>
        <input
          value={tempManwhaUrl}
          onChange={(ev) => setTempManwhaUrl(ev.target.value)}
          style={inputStyles}
        />

        <label style={labelStyles}>Dictionary url:</label>
        <input
          value={tempDictionaryUrl}
          onChange={(ev) => setTempDictionaryUrl(ev.target.value)}
          style={inputStyles}
        />

        <button
          style={{
            marginTop: 20,
            fontSize: 24,
            padding: 10,
            fontWeight: "bolder",
            color: colors.gray,
            cursor: "pointer",
          }}
          onClick={() => {
            if (!tempLanguage) alert("Language is missing");
            if (!tempManwhaUrl) alert("Manwha's is missing");
            if (!tempDictionaryUrl) alert("Dictionary's is missing");

            setLanguage(tempLanguage);
            setManwhaUrl(tempManwhaUrl);
            setDictionaryUrl(tempDictionaryUrl);

            localStorage.setItem("LANGUAGE", tempLanguage);
            localStorage.setItem("MANWHA_URL", tempManwhaUrl);
            localStorage.setItem("DICTIONARY_URL", tempDictionaryUrl);

            setIsOpen(false);
            alert("configs have been updated");
          }}
        >
          Save
        </button>
      </div>
    </Modal>
  );
}

function DictionaryModal({ setIsOpen, isOpen, word, dictionaryUrl }) {
  const [isLoaded, setIsLoaded] = useState(false);

  useEffect(() => {
    if (!isOpen) setIsLoaded(false);
  }, [isOpen]);

  return (
    <Modal
      setIsOpen={setIsOpen}
      width="60%"
      height="80%"
      rootStyle={
        isLoaded && isOpen
          ? { visibility: "visible" }
          : { visibility: "hidden" }
      }
    >
      <iframe
        src={dictionaryUrl + "/" + word}
        title="Embedded Website"
        style={{ width: "100%", height: "100%", border: "none" }}
        allowFullScreen
        onLoad={() => setIsLoaded(true)}
      />
    </Modal>
  );
}

export default App;
