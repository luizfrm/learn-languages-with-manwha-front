export default function Modal({
  setIsOpen,
  children,
  width,
  height,
  rootStyle,
}) {
  return (
    <div
      style={{
        position: "fixed",
        width: "100%",
        height: "100%",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        zIndex: 5,
        ...rootStyle,
      }}
    >
      <div style={{ zIndex: 5, width, height }}>{children}</div>

      <div
        style={{
          position: "absolute",
          backgroundColor: "rgba(0,0,0,.2)",
          width: "100%",
          height: "100%",
          cursor: "pointer",
        }}
        onClick={() => {
          setIsOpen(false);
        }}
      />
    </div>
  );
}
